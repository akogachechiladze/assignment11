package com.example.assignment11repositorypattern

import android.widget.ImageView
import com.bumptech.glide.Glide



fun ImageView.setImage(url: String?) {
    if(!url.isNullOrEmpty()) {
        Glide.with(this).load(url).placeholder(R.mipmap.blackscreen).into(this)
    }
    else{
        setImageResource(R.mipmap.blackscreen)
    }
}