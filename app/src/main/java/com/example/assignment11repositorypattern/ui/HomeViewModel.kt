package com.example.assignment11repositorypattern.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.assignment11repositorypattern.Resource
import com.example.assignment11repositorypattern.model.NotificationsModel
import com.example.assignment11repositorypattern.repository.NotificationsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: NotificationsRepository) : ViewModel() {

    private var _response = MutableSharedFlow<Resource<NotificationsModel.Items>>()
    var response : SharedFlow<Resource<NotificationsModel.Items>> = _response


    fun loadNotifications() {
        viewModelScope.launch {
            repository.getNotifications().onEach {
                _response.emit(it)
            }.launchIn(viewModelScope)
        }
    }
}