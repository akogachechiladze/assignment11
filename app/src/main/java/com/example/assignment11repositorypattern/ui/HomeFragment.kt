package com.example.assignment11repositorypattern.ui

import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment11repositorypattern.Resource
import com.example.assignment11repositorypattern.adapter.NotificationsAdapter
import com.example.assignment11repositorypattern.databinding.HomeFragmentBinding
import com.example.assignment11repositorypattern.model.NotificationsModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeFragmentBinding>(HomeFragmentBinding::inflate) {

    private val viewModel: HomeViewModel by viewModels()
    private lateinit var adapter: NotificationsAdapter

    override fun start() {
        viewModel.loadNotifications()
        collect()
        initRecyclerView()

    }

    private fun initRecyclerView() {
        binding.recycler.layoutManager = LinearLayoutManager(requireContext())
        adapter = NotificationsAdapter()
        binding.recycler.adapter = adapter
    }

    private fun collect() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.response.collect {
                    when (it) {
                        is Resource.Loading -> {binding.swipeRefreshLayout.isRefreshing = true}
                        is Resource.Success -> {adapter.setData(it.data as MutableList<NotificationsModel.Items>)}
                        is Resource.Error -> {
                            Toast.makeText(requireContext(), "${it.message}", Toast.LENGTH_SHORT).show()}
                    }
                }
            }
        }
    }


}