package com.example.assignment11repositorypattern.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment11repositorypattern.R
import com.example.assignment11repositorypattern.databinding.ItemsLayoutBinding
import com.example.assignment11repositorypattern.model.NotificationsModel
import com.example.assignment11repositorypattern.setImage

class NotificationsAdapter(): RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {

    private val items = mutableListOf<NotificationsModel.Items>()


    inner class ViewHolder(private val binding: ItemsLayoutBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun onBind(adapterPosition : Int) {
            val model = items[adapterPosition]
            binding.apply {
                avatarIV.setImage(model.avatar)
                title.setText(model.firstName + " " + model.lastName)

                if (model.isTyping == true)
                    message.setText((R.string.is_typing))
                else if (model.lastMessage == "attachment")
                    message.setText((R.string.sent_an_attachment))
                else if (model.messageType == "voice")
                    message.setText((R.string.sent_a_voice_message))
                else
                    message.setText(model.lastMessage)

                unreadTV.setText(model.unreaMessage!!)



            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemsLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount() = items.size

    @SuppressLint("NotifyDataSetChanged")
    fun setData(items: MutableList<NotificationsModel.Items>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

}