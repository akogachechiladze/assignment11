package com.example.assignment11repositorypattern.repository

import com.example.assignment11repositorypattern.Resource
import com.example.assignment11repositorypattern.model.NotificationsModel
import com.example.assignment11repositorypattern.network.ApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException
import javax.inject.Inject

class NotificationsRepository @Inject constructor(private val apiService: ApiService) {

    suspend fun getNotifications(): Flow<Resource<NotificationsModel.Items>> = flow {
        try {
            emit(Resource.Loading(true))
            val response = apiService.getNotifications()
            val responseBody = response.body()
            if (response.isSuccessful && responseBody != null){
                emit(Resource.Success(responseBody))
            }else {
                emit(Resource.Error(response.message(), null))
            }
        }catch (e: IOException) {
            emit(Resource.Error(e.message, null))
        }
        emit(Resource.Loading(false))
    }


}